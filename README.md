
##
调用创建文件接口，报消息
```json
{
  "error": {
    "message": "The browser (or proxy) sent a request that this server could not understand.",
    "type": "server_error",
    "param": null,
    "code": null
  }
}
```
调用创建会话接口
```json
{
    "error": {
        "message": "You exceeded your current quota, please check your plan and billing details.",
        "type": "insufficient_quota",
        "param": null,
        "code": null
    }
}
```