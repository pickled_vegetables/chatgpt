package org.pickledvegetables.chatgpt.chat.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.pickledvegetables.chatgpt.base.model.MapStringToObject;

import java.math.BigDecimal;
import java.util.List;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class CreateChatCompletionRequest {


    private final String model;

    private final List<ChatMessage> messages;

    private List<ChatFunctions> functions;

    @JsonProperty("function_call")
    private Object functionCall;

    private BigDecimal temperature;

    @JsonProperty("top_p")
    private BigDecimal topP;

    private Integer n;

    private Boolean stream;

    private Object stop;

    @JsonProperty("max_tokens")
    private Integer maxTokens;

    @JsonProperty("presence_penalty")
    private BigDecimal presencePenalty;

    @JsonProperty("frequency_penalty")
    private BigDecimal frequencyPenalty;

    @JsonProperty("logit_bias")
    private MapStringToObject logitBias;

    private String user;

    @Data
    @AllArgsConstructor
    @RequiredArgsConstructor
    public static class ChatMessage {

        private final String role;

        private String content;

        private String name;

        @JsonProperty("function_call")
        private Object functionCall;

    }

    @Data
    @AllArgsConstructor
    @RequiredArgsConstructor
    public static class ChatFunctions {

        private final String name;

        private String description;

        private Object parameters;
    }
}
