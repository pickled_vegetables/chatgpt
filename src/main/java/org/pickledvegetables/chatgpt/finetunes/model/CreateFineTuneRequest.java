package org.pickledvegetables.chatgpt.finetunes.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

/**
 * CreateFineTuneRequest
 */
@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class CreateFineTuneRequest {


    @JsonProperty("training_file")
    private final String trainingFile;

    @JsonProperty("validation_file")
    private String validationFile;

    private String model;

    @JsonProperty("n_epochs")
    private Integer nEpochs;

    @JsonProperty("batch_size")
    private Integer batchSize;

    @JsonProperty("learning_rate_multiplier")
    private BigDecimal learningRateMultiplier;

    @JsonProperty("prompt_loss_weight")
    private BigDecimal promptLossWeight;
    @JsonProperty("compute_classification_metrics")
    private Boolean computeClassificationMetrics;

    @JsonProperty("classification_n_classes")
    private Integer classificationNClasses;
    //
    //This parameter is required for multiclass classification.
    //
    //string
    //Optional
    //Defaults to null
    //The positive class in binary classification.
    @JsonProperty("classification_positive_class")
    private String classificationPositiveClass;
    //
    //This parameter is needed to generate precision, recall, and F1 metrics when doing binary classification.
    //
    //classification_betas
    //array
    //Optional
    //Defaults to null
    //If this is provided, we calculate F-beta scores at the specified beta values. The F-beta score is a generalization of F-1 score. This is only used for binary classification.
    //
    //With a beta of 1 (i.e. the F-1 score), precision and recall are given the same weight. A larger beta score puts more weight on recall and less on precision. A smaller beta score puts more weight on precision and less on recall.
    //
    @JsonProperty("classification_betas")
    private List<String> classificationBetas;
    //suffix
    //string
    //Optional
    //Defaults to null
    //A string of up to 40 characters that will be added to your fine-tuned model name.
    //
    //For example, a suffix of "custom-model-name" would produce a model name like ada:ft-your-org:custom-model-name-2022-02-15-04-21-04.
    private String suffix;
}
