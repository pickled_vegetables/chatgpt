package org.pickledvegetables.chatgpt.edits.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

/**
 * CreateEditResult
 */
@Data
public class CreateEditResult {

    /**
     * The object type.
     */
    private String object;

    /**
     * The creation timestamp.
     */
    private long created;

    /**
     * The choices available for the edit.
     */
    private List<Choice> choices;

    /**
     * The token usage statistics.
     */
    private Usage usage;

    @Data
    public static class Choice {
        /**
         * The text of the choice.
         */
        private String text;

        /**
         * The index of the choice.
         */
        private int index;
    }

    @Data
    public static class Usage {
        /**
         * The number of prompt tokens used.
         */
        @JsonProperty("prompt_tokens")
        private int promptTokens;

        /**
         * The number of completion tokens used.
         */
        @JsonProperty("completion_tokens")
        private int completionTokens;

        /**
         * The total number of tokens used.
         */
        @JsonProperty("total_tokens")
        private int totalTokens;
    }

}
