package org.pickledvegetables.chatgpt.edits.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.math.BigDecimal;

/**
 * CreateEditRequest
 */
@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class CreateEditRequest {

    // Required: ID of the model to use. You can use the text-davinci-edit-001 or code-davinci-edit-001 model with this endpoint.
    private final String model;

    // Optional: Defaults to ''
    // The input text to use as a starting point for the edit.
    private String input;

    // Required: The instruction that tells the model how to edit the prompt.
    private final String instruction;

    // Optional: Defaults to 1
    // How many edits to generate for the input and instruction.
    private Integer n;

    // Optional: Defaults to 1
    // What sampling temperature to use, between 0 and 2.
    // Higher values like 0.8 will make the output more random,
    // while lower values like 0.2 will make it more focused and deterministic.
    // We generally recommend altering this or top_p but not both.
    private BigDecimal temperature;

    // Optional: Defaults to 1
    // An alternative to sampling with temperature, called nucleus sampling,
    // where the model considers the results of the tokens with top_p probability mass.
    // So 0.1 means only the tokens comprising the top 10% probability mass are considered.
    // We generally recommend altering this or temperature but not both.
    @JsonProperty("top_p")
    private BigDecimal topP;

}
