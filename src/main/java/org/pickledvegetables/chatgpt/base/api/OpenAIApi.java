package org.pickledvegetables.chatgpt.base.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import org.pickledvegetables.chatgpt.base.model.BaseError;
import org.pickledvegetables.chatgpt.base.model.MapStringToObject;
import org.pickledvegetables.chatgpt.chat.model.CreateChatCompletionRequest;
import org.pickledvegetables.chatgpt.chat.model.CreateChatCompletionResult;
import org.pickledvegetables.chatgpt.completions.model.CreateCompletionRequest;
import org.pickledvegetables.chatgpt.completions.model.CreateCompletionResult;
import org.pickledvegetables.chatgpt.edits.model.CreateEditRequest;
import org.pickledvegetables.chatgpt.edits.model.CreateEditResult;
import org.pickledvegetables.chatgpt.embeddings.model.CreateEmbeddingsRequest;
import org.pickledvegetables.chatgpt.embeddings.model.CreateEmbeddingsResult;
import org.pickledvegetables.chatgpt.files.model.DeleteFileResult;
import org.pickledvegetables.chatgpt.files.model.ListFileResult;
import org.pickledvegetables.chatgpt.files.model.RetrieveFileResult;
import org.pickledvegetables.chatgpt.files.model.UploadFileRequest;
import org.pickledvegetables.chatgpt.finetunes.model.*;
import org.pickledvegetables.chatgpt.models.constant.UrlPathConstant;
import org.pickledvegetables.chatgpt.models.model.ModelResult;
import org.pickledvegetables.chatgpt.models.model.ModelResultData;
import org.pickledvegetables.chatgpt.moderations.model.CreateModerationRequest;
import org.pickledvegetables.chatgpt.moderations.model.CreateModerationResult;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

/**
 * <a href="https://platform.openai.com/docs/api-reference">OpenAIApi</a>
 */
@Slf4j
public class OpenAIApi {
    /**
     * 配置
     */
    private final Configuration configuration;

    /**
     * 客户端
     */
    private final OkHttpClient okHttpClient;

    /**
     * json转换器
     */
    private final ObjectMapper objectMapper = new ObjectMapper();


    /**
     * 媒体类型
     */
    private final MediaType mediaType = MediaType.parse("application/json");

    private final String projectVersion = "2.0.0-SNAPSHOT";

    public OpenAIApi(Configuration configuration) {
        this.configuration = configuration;
        this.okHttpClient = new OkHttpClient.Builder().build();
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    }


    private Request.Builder getRequestBuilder() {
        Headers.Builder builder = new Headers.Builder().add("OpenAI-Organization", this.configuration.getOrganization()).add("User-Agent", "OpenAI/Java/" + this.projectVersion);
        if (this.configuration.isAzureType()) {
            builder.add("api-key", this.configuration.getApiKey());
        } else {
            builder.add("Authorization", "Bearer " + this.configuration.getApiKey());
        }
        return new Request.Builder().headers(builder.build());
    }

    private <D> CompletableFuture<D> postToData(Object requestBodyObject, String relationUrl, Class<D> dataClass) {
        try {
            return requestToData(getRequestBuilder().url(this.configuration.getBasePath() + relationUrl).post(createRequestBody(requestBodyObject)).build(), dataClass);
        } catch (JsonProcessingException e) {
            return newFailedFuture(e);
        }
    }

    private <D> CompletableFuture<D> postFormData(Object requestBodyObject, String relationUrl, Class<D> dataClass) {
        try {
            Map<String, Object> map = this.objectMapper.readValue(this.objectMapper.writeValueAsString(requestBodyObject), MapStringToObject.class);
            FormBody.Builder formBodyBuilder = new FormBody.Builder();
            map.forEach((key, value) -> formBodyBuilder.add(key, value + ""));
            FormBody formBody = formBodyBuilder.build();
            return requestToData(getRequestBuilder().url(this.configuration.getBasePath() + relationUrl).post(formBody).build(), dataClass);
        } catch (JsonProcessingException e) {
            return newFailedFuture(e);
        }
    }

    private <D> CompletableFuture<D> newFailedFuture(Exception e) {
        CompletableFuture<D> completableFuture = new CompletableFuture<>();
        completableFuture.completeExceptionally(e);
        return completableFuture;
    }

    private <D> CompletableFuture<D> requestToData(Request request, Class<D> dataClass) {
        ResponseBody responseBody;

        String string;
        try (Response response = okHttpClient.newCall(request).execute()) {
            responseBody = response.body();
            string = responseBody != null ? responseBody.string() : null;
            log.info(string);
        } catch (IOException e) {
            return newFailedFuture(e);
        }
        Map<String, Object> map;
        try {
            map = objectMapper.readValue(string, MapStringToObject.class);
        } catch (JsonProcessingException e) {
            return newFailedFuture(e);
        }
        if (map.get("error") != null) {
            try {
                objectMapper.readValue(string, BaseError.class);
                log.error("请求失败，请求路径是：{}，具体消息是：{}。", request.url(), string);
                return newFailedFuture(new Exception(string));
            } catch (JsonProcessingException e) {
                return newFailedFuture(e);
            }
        }
        D result;
        try {
            result = objectMapper.readValue(string, dataClass);
        } catch (JsonProcessingException e) {
            return newFailedFuture(e);
        }
        return CompletableFuture.completedFuture(result);
    }

    private RequestBody createRequestBody(Object object) throws JsonProcessingException {
        String create = this.objectMapper.writeValueAsString(object);
        log.info(create);
        return RequestBody.create(create, mediaType);
    }

    /**
     * <a href="https://platform.openai.com/docs/api-reference/models/list">列出模型</a>
     * 列出当前可用的模型，并提供有关每个模型的基本信息，例如所有者和可用性。
     *
     * @return 所有模型
     */
    public CompletableFuture<ModelResult> listModels() {
        Request request = getRequestBuilder().url(this.configuration.getBasePath() + UrlPathConstant.MODELS).build();
        return requestToData(request, ModelResult.class);
    }

    /**
     * <a href="https://platform.openai.com/docs/api-reference/models/retrieve">检索模型</a>
     * 检索模型实例，提供有关模型的基本信息，例如所有者和权限。
     *
     * @param model 用于此请求的模型的 ID
     * @return 模型的基本信息
     */
    public CompletableFuture<ModelResultData> retrieveModel(String model) {
        Request request = getRequestBuilder().url(this.configuration.getBasePath() + UrlPathConstant.MODELS + "/" + model).build();
        return requestToData(request, ModelResultData.class);
    }


    /**
     * <a href="https://platform.openai.com/docs/api-reference/chat/completions/create">创建会话</a>
     * Creates a model response for the given chat conversation.
     */
    public CompletableFuture<CreateChatCompletionResult> createChatCompletion(CreateChatCompletionRequest createChatCompletionRequest) {

        return postToData(createChatCompletionRequest, this.configuration.isAzureType() ? UrlPathConstant.AZURE_CHAT : UrlPathConstant.CHAT, CreateChatCompletionResult.class);
    }


    /**
     * <a href="https://platform.openai.com/docs/api-reference/completions/create">完成</a>
     * 为提供的提示和参数创建补全。
     */
    public CompletableFuture<CreateCompletionResult> createCompletion(CreateCompletionRequest createCompletionRequest) {

        return postToData(createCompletionRequest, UrlPathConstant.COMPLETIONS, CreateCompletionResult.class);
    }

    /**
     * <a href="https://platform.openai.com/docs/api-reference/edits">Edits</a>
     * Given a prompt and an instruction, the model will return an edited version of the prompt.
     */
    public CompletableFuture<CreateEditResult> createEdit(CreateEditRequest createEditRequest) {
        return postToData(createEditRequest, UrlPathConstant.EDITS, CreateEditResult.class);
    }

    /**
     * <a href="https://platform.openai.com/docs/api-reference/embeddings">Embeddings</a>
     * Get a vector representation of a given input that can be easily consumed by machine learning models and algorithms.
     */
    public CompletableFuture<CreateEmbeddingsResult> createEmbedding(CreateEmbeddingsRequest createEmbeddingsRequest) {
        return postToData(createEmbeddingsRequest, UrlPathConstant.EMBEDDINGS, CreateEmbeddingsResult.class);
    }

    /**
     * <a href="https://platform.openai.com/docs/api-reference/files">Files</a>
     * Files are used to upload documents that can be used with features like Fine-tuning.
     */
    public CompletableFuture<ListFileResult> listFiles() {
        return requestToData(getRequestBuilder().url(this.configuration.getBasePath() + UrlPathConstant.FILES).build(), ListFileResult.class);
    }

    /**
     * <a href="https://platform.openai.com/docs/api-reference/files">Files</a>
     * Files are used to upload documents that can be used with features like Fine-tuning.
     */
    public CompletableFuture<ListFileResult> createFile(UploadFileRequest uploadFileRequest) {
        return postFormData(uploadFileRequest, UrlPathConstant.FILES, ListFileResult.class);
    }

    /**
     * <a href="https://platform.openai.com/docs/api-reference/files/delete">Delete file</a>
     * Delete a file.
     */
    public CompletableFuture<DeleteFileResult> deleteFile(String fileId) {
        return requestToData(getRequestBuilder().url(this.configuration.getBasePath() + UrlPathConstant.FILES + "/" + fileId).delete().build(), DeleteFileResult.class);
    }

    /**
     * <a href="https://platform.openai.com/docs/api-reference/files/retrieve">Retrieve file</a>
     * Returns information about a specific file.
     */
    public CompletableFuture<RetrieveFileResult> retrieveFile(String fileId) {
        return requestToData(getRequestBuilder().url(this.configuration.getBasePath() + UrlPathConstant.FILES + "/" + fileId).get().build(), RetrieveFileResult.class);
    }

    /**
     * <a href="https://platform.openai.com/docs/api-reference/files/retrieve-content">Retrieve content file</a>
     * Returns information about a specific file.
     */
    public CompletableFuture<Byte[]> retrieveFileContent(String fileId) {
        return requestToData(getRequestBuilder().url(this.configuration.getBasePath() + UrlPathConstant.FILES + "/" + fileId + "/content").get().build(), Byte[].class);
    }

    /**
     * <a href="https://platform.openai.com/docs/api-reference/fine-tunes/create">Create fine-tune</a>
     * Creates a job that fine-tunes a specified model from a given dataset.
     * Response includes details of the enqueued job including job status and the name of the fine-tuned models once complete.
     */
    public CompletableFuture<CreateFineTuneResult> createFineTune(CreateFineTuneRequest createFineTuneRequest) {
        return postFormData(createFineTuneRequest, UrlPathConstant.FINE_TUNES, CreateFineTuneResult.class);
    }

    /**
     * <a href="https://platform.openai.com/docs/api-reference/fine-tunes/list">list fine-tunes</a>
     * List your organization's fine-tuning jobs
     */
    public CompletableFuture<ListFineTuneResult> listFineTunes() {
        return requestToData(getRequestBuilder().url(this.configuration.getBasePath() + UrlPathConstant.FINE_TUNES).get().build(), ListFineTuneResult.class);
    }

    /**
     * <a href="https://platform.openai.com/docs/api-reference/fine-tunes/retrieve">Retrieve fine-tune</a>
     * Gets info about the fine-tune job.
     */
    public CompletableFuture<RetrieveFineTuneResult> retrieveFineTune(String fineTuneId) {
        return requestToData(getRequestBuilder().url(this.configuration.getBasePath() + UrlPathConstant.FINE_TUNES + "/" + fineTuneId).get().build(), RetrieveFineTuneResult.class);
    }

    /**
     * <a href="https://platform.openai.com/docs/api-reference/fine-tunes/cancel">Cancel fine-tune</a>
     * Immediately cancel a fine-tune job.
     */
    public CompletableFuture<CancelFineTuneResult> cancelFineTune(String fineTuneId) {
        return requestToData(getRequestBuilder().url(this.configuration.getBasePath() + UrlPathConstant.FINE_TUNES + "/" + fineTuneId + "/cancel").get().build(), CancelFineTuneResult.class);
    }

    /**
     * <a href="https://platform.openai.com/docs/api-reference/fine-tunes/events">List fine-tune events</a>
     * Get fine-grained status updates for a fine-tune job.
     */
    public CompletableFuture<ListFineTuneEventsResult> listFineTuneEvents(String fineTuneId, Boolean stream) {

        HttpUrl.Builder builder = HttpUrl.get(this.configuration.getBasePath() + UrlPathConstant.FINE_TUNES + "/" + fineTuneId + "/events").newBuilder();
        if (stream != null) {
            builder.addQueryParameter("stream", stream.toString());
        }
        HttpUrl httpUrl = builder.build();
        return requestToData(getRequestBuilder().url(httpUrl).get().build(), ListFineTuneEventsResult.class);
    }

    /**
     * <a href="https://platform.openai.com/docs/api-reference/fine-tunes/delete-model">Delete fine-tune model</a>
     * Delete a fine-tuned model. You must have the Owner role in your organization.
     */
    public CompletableFuture<DeleteFineTuneModelResult> deleteModel(String model) {

        return requestToData(getRequestBuilder().url(this.configuration.getBasePath() + UrlPathConstant.MODELS + "/" + model).delete().build(), DeleteFineTuneModelResult.class);
    }

    /**
     * <a href="https://platform.openai.com/docs/api-reference/moderations/create">Create moderation</a>
     * Classifies if text violates OpenAI's Content Policy
     */
    public CompletableFuture<CreateModerationResult> createModeration(CreateModerationRequest createModerationRequest) {

        return postToData(createModerationRequest, UrlPathConstant.MODERATIONS, CreateModerationResult.class);
    }


}
