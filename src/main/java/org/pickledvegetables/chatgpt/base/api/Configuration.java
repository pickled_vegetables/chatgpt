package org.pickledvegetables.chatgpt.base.api;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Configuration
 */
@Data
@AllArgsConstructor
public class Configuration {

    /**
     * parameter for apiKey security
     */
    private String apiKey;

    /**
     * OpenAI organization id
     */
    private String organization;
    /**
     * parameter for basic security
     */
    private String username;
    /**
     * parameter for basic security
     */
    private String password;
    /**
     * parameter for oauth2 security
     */
    private String accessToken;

    /**
     * override base path
     */
    private String basePath = "https://api.openai.com/";

    /**
     * azure类型
     */
    private boolean azureType;

    public Configuration(String apiKey, String organization) {
        this.apiKey = apiKey;
        this.organization = organization;
    }

    public Configuration(String apiKey, String organization, String basePath, boolean azureType) {
        this.apiKey = apiKey;
        this.organization = organization;
        this.basePath = basePath;
        this.azureType = azureType;
    }
}
