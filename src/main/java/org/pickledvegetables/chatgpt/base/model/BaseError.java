package org.pickledvegetables.chatgpt.base.model;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * BaseError
 */
@Data
public class BaseError {
    private ErrorDetails error;

    @Data
    public static class ErrorDetails {
        private String message;
        private String type;
        private String param;
        private String code;
    }
}
