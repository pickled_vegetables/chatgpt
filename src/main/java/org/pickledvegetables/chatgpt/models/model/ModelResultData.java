package org.pickledvegetables.chatgpt.models.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

/**
 * ModelResultData
 */
@Data
public class ModelResultData {

    private String id;

    private String object;

    @JsonProperty("owned_by")
    private String ownedBy;

    private Long created;

    private String root;

    private String parent;

    private List<Permission> permission;

}
