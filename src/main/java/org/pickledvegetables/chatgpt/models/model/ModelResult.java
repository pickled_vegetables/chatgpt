package org.pickledvegetables.chatgpt.models.model;

import lombok.Data;

import java.util.List;

/**
 * ListModelsResult
 */
@Data
public class ModelResult {

    private List<ModelResultData> data;

    private String object;
}
