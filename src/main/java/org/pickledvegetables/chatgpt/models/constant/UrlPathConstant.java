package org.pickledvegetables.chatgpt.models.constant;

/**
 * UrlPathConstant
 */
public class UrlPathConstant {

    /**
     * models
     */
    public static final String MODELS = "models";
    /**
     * chat
     */
    public static final String CHAT = "v1/chat/completions";
    /**
     * chat
     */
    public static final String AZURE_CHAT = "openai/deployments/gpt-35-turbo/chat/completions?api-version=2023-03-15-preview";
    /**
     * completions
     */
    public static final String COMPLETIONS = "v1/completions";
    /**
     * edits
     */
    public static final String EDITS = "v1/edits";
    /**
     * embeddings
     */
    public static final String EMBEDDINGS = "v1/embeddings";
    /**
     * files
     */
    public static final String FILES = "v1/files";
    /**
     * fine-tunes
     */
    public static final String FINE_TUNES = "v1/fine-tunes";
    /**
     * moderations
     */
    public static final String MODERATIONS = "v1/moderations";
}
