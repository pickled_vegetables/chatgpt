package org.pickledvegetables.chatgpt.files.model;

import lombok.Data;
import lombok.RequiredArgsConstructor;

/**
 * UploadFileRequest
 */
@Data
@RequiredArgsConstructor
public class UploadFileRequest {
    /**
     * Name of the JSON Lines file to be uploaded.
     * <p>
     * If the is set to "fine-tune", each line is a JSON record with "prompt" and "completion" fields representing your training examples.
     */
    private final String file;

    /**
     * The intended purpose of the uploaded documents.
     * <p>
     * Use "fine-tune" for Fine-tuning. This allows us to validate the format of the uploaded file.
     */
    private final String purpose;
}
