package org.pickledvegetables.chatgpt.files.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * RetrieveFileResult
 */
@Data
public class RetrieveFileResult {

    private String id;
    private String object;
    private Integer bytes;

    @JsonProperty("created_at")
    private LocalDateTime createdAt;
    private String filename;
    private String purpose;

}
