package org.pickledvegetables.chatgpt.files.model;

import lombok.Data;

/**
 * DeleteFileResult
 */
@Data
public class DeleteFileResult {
    /**
     * The ID of the file.
     */
    private String id;

    /**
     * The object type of the file.
     */
    private String object;

    /**
     * Indicates whether the file is deleted or not.
     */
    private boolean deleted;

}
