package org.pickledvegetables.chatgpt.files.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

/**
 * ListFileResult
 */
@Data
public class ListFileResult {

    /**
     * The list of files.
     */
    private List<FileData> data;

    /**
     * The object type.
     */
    private String object;

    @Data
    public static class FileData {
        /**
         * The ID of the file.
         */
        private String id;

        /**
         * The object type of the file.
         */
        private String object;

        /**
         * The size of the file in bytes.
         */
        private int bytes;

        /**
         * The creation timestamp of the file.
         */
        @JsonProperty("created_at")
        private long createdAt;

        /**
         * The filename.
         */
        private String filename;

        /**
         * The purpose of the file.
         */
        private String purpose;
    }

}
