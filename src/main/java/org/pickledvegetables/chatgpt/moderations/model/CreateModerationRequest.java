package org.pickledvegetables.chatgpt.moderations.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

/**
 * CreateModerationRequest
 */
@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class CreateModerationRequest {

    //input
    //string or array
    //Required
    //The input text to classify
    //
    private final Object input;
    //model
    //string
    //Optional
    //Defaults to text-moderation-latest
    //Two content moderations models are available: text-moderation-stable and text-moderation-latest.
    //
    //The default is text-moderation-latest which will be automatically upgraded over time. This ensures you are always using our most accurate model. If you use text-moderation-stable, we will provide advanced notice before updating the model. Accuracy of text-moderation-stable may be slightly lower than for text-moderation-latest
    private String model;
}
