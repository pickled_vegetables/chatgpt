package org.pickledvegetables.chatgpt.embeddings.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.List;

/**
 * CreateEmbeddingsResult
 */
@Data
public class CreateEmbeddingsResult {

    /**
     * The object type.
     */
    private String object;

    /**
     * The list of embedding data.
     */
    private List<EmbeddingData> data;

    /**
     * The model type.
     */
    private String model;

    /**
     * The token usage statistics.
     */
    private Usage usage;

    @Data
    public static class EmbeddingData {
        /**
         * The object type of the embedding.
         */
        private String object;

        /**
         * The embedding values.
         */
        private List<Double> embedding;

        /**
         * The index of the embedding.
         */
        private int index;
    }

    @Data
    public static class Usage {
        /**
         * The number of prompt tokens used.
         */
        @JsonProperty("prompt_tokens")
        private int promptTokens;

        /**
         * The total number of tokens used.
         */
        @JsonProperty("total_tokens")
        private int totalTokens;
    }


}

