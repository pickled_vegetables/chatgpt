package org.pickledvegetables.chatgpt.embeddings.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

/**
 * CreateEmbeddingsRequest
 */
@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class CreateEmbeddingsRequest {

    /**
     * ID of the model to use. You can use the List models API to see all of your available models, or see our Model overview for descriptions of them.
     */
    private final String model;

    /**
     * Input text to embed, encoded as a string or array of tokens.
     * To embed multiple inputs in a single request, pass an array of strings or array of token arrays.
     * Each input must not exceed the max input tokens for the model (8191 tokens for text-embedding-ada-002).
     * Example Python code for counting tokens.
     */
    private final Object input;

    /**
     * A unique identifier representing your end-user, which can help OpenAI to monitor and detect abuse.
     * Learn more.
     */
    private String user;
}

