package org.pickledvegetables.chatgpt.completions.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.math.BigDecimal;
import java.util.Map;

/**
 * CreateCompletionRequest
 */
@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class CreateCompletionRequest {

    private final String model;

    private final String prompt;

    private String suffix;

    @JsonProperty("max_tokens")
    private Integer maxTokens;

    private BigDecimal temperature;

    @JsonProperty("top_p")
    private BigDecimal topP;

    private Integer n;

    private Boolean stream;

    private Integer logprobs;

    private Boolean echo;

    private String stop;

    @JsonProperty("presence_penalty")
    private BigDecimal presencePenalty;

    @JsonProperty("frequency_penalty")
    private BigDecimal frequencyPenalty;

    @JsonProperty("best_of")
    private Integer bestOf;

    @JsonProperty("logit_bias")
    private Map<String, Integer> logitBias;

    @JsonProperty("user")
    private String user;
}
