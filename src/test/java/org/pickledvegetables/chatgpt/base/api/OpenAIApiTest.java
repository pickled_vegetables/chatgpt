package org.pickledvegetables.chatgpt.base.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.pickledvegetables.chatgpt.chat.model.CreateChatCompletionRequest;
import org.pickledvegetables.chatgpt.completions.model.CreateCompletionRequest;
import org.pickledvegetables.chatgpt.embeddings.model.CreateEmbeddingsRequest;
import org.pickledvegetables.chatgpt.files.model.UploadFileRequest;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * OpenAIApiTest
 */
class OpenAIApiTest {

    private static String BASE_PATH;
    private static String OPENAI_API_KEY;
    private static String ORGANIZATION_ID;
    private static boolean AZURE_TYPE;

    @BeforeAll
    public static void before() {

        try (InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("application.properties")) {
            Properties properties = new Properties();
            properties.load(inputStream);
            BASE_PATH = properties.getProperty("pickledvegetables.chatgpt.base-path");
            OPENAI_API_KEY = properties.getProperty("pickledvegetables.chatgpt.openapi-key");
            ORGANIZATION_ID = properties.getProperty("pickledvegetables.chatgpt.organization-id");
            AZURE_TYPE = Boolean.parseBoolean(properties.getProperty("pickledvegetables.chatgpt.azure-type"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    //    @Test
    void listModels() {

        OpenAIApi openai = new OpenAIApi(new Configuration(OPENAI_API_KEY, ORGANIZATION_ID, BASE_PATH, AZURE_TYPE));
        System.out.println(Assertions.assertDoesNotThrow(() -> new ObjectMapper().writeValueAsString(openai.listModels().join())));
    }


    void retrieveModel() {

        OpenAIApi openai = new OpenAIApi(new Configuration(OPENAI_API_KEY, ORGANIZATION_ID, BASE_PATH, AZURE_TYPE));
        System.out.println(Assertions.assertDoesNotThrow(() -> new ObjectMapper().writeValueAsString(openai.retrieveModel("text-davinci-003").join())));
    }

//    @Test
    void createChatCompletion() {

        //"messages": [{"role": "system", "content": "You are a helpful assistant."}, {"role": "user", "content": "Hello!"}]
        OpenAIApi openai = new OpenAIApi(new Configuration(OPENAI_API_KEY, ORGANIZATION_ID, BASE_PATH, AZURE_TYPE));

        List<CreateChatCompletionRequest.ChatMessage> chatMessages = new ArrayList<>();
        chatMessages.add(new CreateChatCompletionRequest.ChatMessage("system", "You are a helpful assistant.", null, null));
        chatMessages.add(new CreateChatCompletionRequest.ChatMessage("user", "Hello!", null, null));
        System.out.println(Assertions.assertDoesNotThrow(() -> new ObjectMapper().writeValueAsString(openai.createChatCompletion(new CreateChatCompletionRequest("gpt-3.5-turbo", chatMessages)).join())));
    }

    void createCompletion() {

        OpenAIApi openai = new OpenAIApi(new Configuration(OPENAI_API_KEY, ORGANIZATION_ID));
        System.out.println(Assertions.assertDoesNotThrow(() -> new ObjectMapper().writeValueAsString(openai.createCompletion(new CreateCompletionRequest("text-davinci-003", "Say this is a test")).join())));
    }

    void createEmbedding() {

        OpenAIApi openai = new OpenAIApi(new Configuration(OPENAI_API_KEY, ORGANIZATION_ID));
        System.out.println(Assertions.assertDoesNotThrow(() -> new ObjectMapper().writeValueAsString(openai.createEmbedding(new CreateEmbeddingsRequest("text-embedding-ada-002", "The food was delicious and the waiter...")).join())));
    }


    void listFiles() {

        OpenAIApi openai = new OpenAIApi(new Configuration(OPENAI_API_KEY, ORGANIZATION_ID));
        System.out.println(Assertions.assertDoesNotThrow(() -> new ObjectMapper().writeValueAsString(openai.listFiles().join())));
    }

    void createFile() {

        OpenAIApi openai = new OpenAIApi(new Configuration(OPENAI_API_KEY, ORGANIZATION_ID));
        System.out.println(Assertions.assertDoesNotThrow(() -> new ObjectMapper().writeValueAsString(openai.createFile(new UploadFileRequest("/Users/chixuehui/mydata.json", "fine-tune")).join())));
    }
}